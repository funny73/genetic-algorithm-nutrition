package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class Nutrition {
    private ArrayList<Product> allproducts = null;
    private ArrayList<ArrayList<Product>> nutritions = new ArrayList<ArrayList<Product>>();
    private double fatsNorm;
    private double carbsNorm;
    private double proteinsNorm;
    private double calsNorm;
    private double accuracy;
    private int generation = 1;
    Nutrition(ArrayList<Product> _allproducts, double _fatsNorm, double _carbsNorm, double _proteinsNorm, double _calsNorm, double _accuracy){
        allproducts = _allproducts;
        fatsNorm = _fatsNorm;
        carbsNorm = _carbsNorm;
        proteinsNorm = _proteinsNorm;
        calsNorm = _calsNorm;
        accuracy = _accuracy;
    }
    private void createStartNutritions(){
        Random rand = new Random();

        for(int i = 0; i < allproducts.size() * 2; i++){
            ArrayList<Product> curNutrition = new ArrayList<Product>();
            for (Product product: allproducts
                 ) {
                if(rand.nextBoolean()){
                    curNutrition.add(product);
                }
            }
            nutritions.add(curNutrition);
        }
    }

    private double CalcTargetFunctionValue(ArrayList<Product> nutrition){
        double totalFats = 0;
        double totalCarbs = 0;
        double totalProteins = 0;
        double totalCals = 0;
        for (Product product: nutrition
             ) {
            totalCarbs += product.getCarbs();
            totalFats += product.getFats();
            totalProteins += product.getProteins();
            totalCals += product.getCallories();
        }
        double value = Math.abs(totalFats - fatsNorm) + Math.abs(totalCarbs - carbsNorm) + Math.abs(totalProteins - proteinsNorm) + Math.abs(totalCals - calsNorm);
       // for (Product product: nutrition
       // ) {
       //     System.out.print(" " + product.getName());
       // }
        //System.out.print(" " + value);
        //System.out.println();
        return value;
    }

    Comparator<ArrayList<Product>> comp = new Comparator<ArrayList<Product>>() {
        @Override
        public int compare(ArrayList<Product> nutrition1, ArrayList<Product> nutrition2) {
            if (CalcTargetFunctionValue(nutrition1) > CalcTargetFunctionValue(nutrition2)){
                return 1;
            }
            return 0;

        }
    };

    private boolean check(ArrayList<Product> nutrition){
        double totalFats = 0;
        double totalCarbs = 0;
        double totalProteins = 0;
        double totalCals = 0;

        for (Product product: nutrition
        ) {
            totalCarbs += product.getCarbs();
            totalFats += product.getFats();
            totalProteins += product.getProteins();
            totalCals += product.getCallories();
        }

        if(Math.abs(totalCarbs - carbsNorm)/carbsNorm > accuracy){
            return false;
        }

        if(Math.abs(totalCals - calsNorm)/calsNorm > accuracy){
            return false;
        }

        if(Math.abs(totalFats - fatsNorm)/fatsNorm > accuracy){
            return false;
        }

        if(Math.abs(totalProteins - proteinsNorm)/proteinsNorm > accuracy){
            return false;
        }
        return true;
    }
    private  void mutate(){
        Random random = new Random();
        for ( int i = 2; i < nutritions.size(); i++
             ) {


            for (Product product : allproducts
            ) {
                if (random.nextFloat() > 0.8) {
                    if(nutritions.get(i).contains(product))
                        nutritions.get(i).remove(product);
                    else
                        nutritions.get(i).add(product);
                }

            }
        }
    }
    private  void crossover(){
        Random random = new Random();
        ArrayList<ArrayList<Product>> newNutritions = new ArrayList<ArrayList<Product>>();
        for(int i = 0; i < nutritions.size()/2; i++){
            ArrayList<Product> firstParent = nutritions.get(random.nextInt(nutritions.size()/2));
            ArrayList<Product> secondParent = nutritions.get(random.nextInt(nutritions.size()/2));
            ArrayList<Product> newNutrition = new ArrayList<Product>();
            for (Product product: allproducts
                 ) {
                if(random.nextBoolean()){
                    if(firstParent.contains(product))
                        newNutrition.add(product);
                }else {
                    if(secondParent.contains(product))
                        newNutrition.add(product);
                }
            }
            newNutritions.add(newNutrition);
        }
        newNutritions.add( nutritions.get(0));
        newNutritions.add( nutritions.get(1));
        newNutritions.add( nutritions.get(2));
        newNutritions.add( nutritions.get(3));
        newNutritions.add( nutritions.get(4));
        newNutritions.add( nutritions.get(5));
        //newNutritions.add( nutritions.get(6));
        //newNutritions.add( nutritions.get(7));
        //newNutritions.add( nutritions.get(8));
        //newNutritions.add( nutritions.get(9));

        nutritions = newNutritions;
    }

    public ArrayList<Product> getNutrition(){
        createStartNutritions();
        nutritions.sort(comp);
        System.out.println(generation + " generation " + CalcTargetFunctionValue(nutritions.get(0)));
        System.out.println(generation + " generation " + CalcTargetFunctionValue(nutritions.get(1)));
        System.out.println(generation++ + " generation " + CalcTargetFunctionValue(nutritions.get(2)));

        while (!check(nutritions.get(0))){
            System.out.println(generation + " generation " + CalcTargetFunctionValue(nutritions.get(0)));
            System.out.println(generation + " generation " + CalcTargetFunctionValue(nutritions.get(1)));
            System.out.println(generation++ + " generation " + CalcTargetFunctionValue(nutritions.get(2)));
            crossover();
            mutate();
            nutritions.sort(comp);

        }
        return  nutritions.get(0);
    }
}
