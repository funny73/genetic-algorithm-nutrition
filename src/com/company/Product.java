package com.company;

public class Product {
    private double proteins;
    private double fats;
    private double carbs;
    private String name;
    Product(double _proteins, double _fats, double _carbs, String _name){
        proteins = _proteins;
        fats = _fats;
        carbs = _carbs;
        name = _name;
    }
    public double getCallories() {
        return fats * 9 + proteins * 4 + carbs * 4;
    }
    public double getFats() {
        return fats;
    }

    public double getCarbs() {
        return carbs;
    }

    public double getProteins() {
        return proteins;
    }

    public String getName() {
        return name;
    }
}
