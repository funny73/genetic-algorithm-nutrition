package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Product apple = new Product(0.4 ,0.4,9.8, "apple");
        Product egg = new Product(6.3 ,5.3,0.56, "egg");
        Product oatmeal = new Product(6.5 ,3.1,33, "oatmeal");
        Product orangeJuce = new Product(0 ,0,12, "orange juce");
        Product yogurt = new Product(4 ,2.7,6.8, "yogurt");
        Product bacon = new Product(16 ,40,0, "bacon");
        Product ryeBread = new Product(8.5 ,3.3,48.3,"rye bread");

        ArrayList<Product> allproducts = new ArrayList<Product>();
        allproducts.add(apple);
        allproducts.add(egg);
        allproducts.add(oatmeal);
        allproducts.add(orangeJuce);
        allproducts.add(bacon);
        allproducts.add(yogurt);
        allproducts.add(ryeBread);

        Nutrition nutrition = new Nutrition(allproducts, 25,60,34,600,0.5   );
        ArrayList<Product> res = nutrition.getNutrition();

        for (Product product: res
             ) {
            System.out.print(" " + product.getName());
        }
    }
}
